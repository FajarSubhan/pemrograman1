(*
 * @Title       : Konsep Dasar Perulangan Bersarang Dengan For Do 
 * @Matkul		: Pemrograman
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)


program nested_loop;
uses crt; // crt unit library


var 
  i , j : integer;

begin 
clrscr;

(*
 * @format dasar perulangan bersarang dengan for do 
 * @sebelum membaca ini minimal sudah harus tahu dulu [konsep] dasar perulangan for do,biar gak puyeng (^_^) 
 * ================================== Format Dasar =============================================
 * for variabel_counter_1 := nilai_awal_1 to nilai_akhir_1 do
 *	begin
 *	  kode program..1
 *	  kode program..1
 *  for variabel_counter_2 := nilai_awal_2 to nilai_akhir_2 do
 *   begin
 *     kode program..2
 *     kode program..2
 *   end;
 *    kode program..1
 *    kode program..1
 *  end;
 *
 * untuk penamaan variable counter biasanya pake i,j,k atau x,y,z tapi selain ini bisa juga pake indentifier lainya
 * beberapa programmer biasanya pake ini dalam pengerjaannya.
*)

for i := 1 to 3 do 
begin 
  for j := 1 to 3 do 
  begin 
    writeln('i luar: ',i,' dan j dalam: ',j);
  end;
end;

{
hasil nya :

i luar: 1 dan j dalam: 1
i luar: 1 dan j dalam: 2
i luar: 1 dan j dalam: 3
i luar: 2 dan j dalam: 1
i luar: 2 dan j dalam: 2
i luar: 2 dan j dalam: 3
i luar: 3 dan j dalam: 1
i luar: 3 dan j dalam: 2
i luar: 3 dan j dalam: 3
}

(*
 * karena perulangan yang (var j) itu ada di dalam ,maka setiap kali (var i) diproses,
 * perulangan var j juga akan di proses seluruhnya.
 *
 * misalnya yang di luar for i := 1 to 4 do , dan yang didalam misalnya for j := 1 - 3 do 
 * berarti hasilnya akan seperti ini : 
 
i luar: 1 dan j dalam: 1
i luar: 1 dan j dalam: 2
i luar: 1 dan j dalam: 3
i luar: 2 dan j dalam: 1
i luar: 2 dan j dalam: 2
i luar: 2 dan j dalam: 3
i luar: 3 dan j dalam: 1
i luar: 3 dan j dalam: 2
i luar: 3 dan j dalam: 3
i luar: 4 dan j dalam: 1
i luar: 4 dan j dalam: 2
i luar: 4 dan j dalam: 3
*)
end.