(*
 * @Title       : Program Pendaftaran Siswa SMPN 1 Jaya Selalu
 * @Matkul		: Pemrograman
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)

program pendaftaran_siswa;
uses crt; // import library crt.tpu.unit 

{ ==== Start Deklarasi Variable ===== }
var 
  data_siswa_arr : array of integer;
var 
  i,x : integer;
  
{ ==== End Deklarasi Variable ===== }
begin 
clrscr;

setlength(data_siswa_arr,4);

writeln('=== Program Pendaftaran Siswa SMPN 1 Jaya Selalu ===');
writeln('===============================');
writeln;


write('masukan banyaknya gelombang pendaftaran : ');
readln(i);

write('Data Siswa Pada Gelombang 1 : ');
readln(data_siswa_arr[1]);

write('Data Siswa Pada Gelombang 2 : ');
readln(data_siswa_arr[2]);

write('Data Siswa Pada Gelombang 3 : ');
readln(data_siswa_arr[3]);

{ Start For To Do }
for x := 1 to i do 
begin 
  writeln('==============================');
  writeln('| Gelombang Ke ', x , ' yaitu ',data_siswa_arr[x] , '    |');
  writeln('==============================');
end;
{ End For To Do }

end.
