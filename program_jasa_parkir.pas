(*
 * @Title       : Program Jasa Parkir
 * @Matkul		: Pemrograman
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)

program jasa_parkir;
uses crt; // import library crt.tpu.unit 

{ ==== Start Deklarasi Variable ===== }
var 
  nama_lenkap,jenis_kendaraan,plat 							: string;
var 
  jam_masuk,jam_keluar,total_jam,tarif,uang_masuk			: integer;
var 
  kategori 			: char;
var 
  harga,kembalian	: real;
{ ==== End Deklarasi Variable ===== }

{ === Start Main Program ====}
begin
clrscr;

writeln('============= SELAMAT DATANG DI PROGRAM JASA PARKIR ===============');
writeln('===================================================================');

{ * input data * }

// Nama Lengkap
write('masukkan nama lengkap anda          : ');
readln(nama_lenkap);


// Jenis Kendaraan
write('pilih jenis kendaraan [MOTOR/MOBIL] : ');
readln(jenis_kendaraan);

// plat nomor
write('masukan plat nomor anda             : ');
readln(plat);

// jam masuk
write('jam masuk   : ');
readln(jam_masuk);

// jam keluar 
write('jam keluar  : ');
readln(jam_keluar);

// total jam : jam keluar parkir di kurang jam masuk nya
total_jam := jam_keluar - jam_masuk;
writeln('Total Jam : ',total_jam);

// kategori 
writeln('=============================');
writeln('Pilih Kategori Lama Jam');
writeln('=============================');
writeln('A = 1 - 5 jam');
writeln('B = 6 - 10 jam');
writeln('C = 11 - 24 jam');
write('Kategori : ');
readln(kategori);

(* Output Data *) 
writeln('==============================');
writeln('Nama            : ',nama_lenkap);
writeln('nomor kendaraan : ',plat);
writeln('jenis kendaraan : ',jenis_kendaraan);
writeln('==============================');


{*
 * FYI 
 * --- hitung tarif ---
 * Mobil = Rp.5.000
 * Motor = Rp.2.000
*}
IF( jenis_kendaraan = 'mobil' ) then 
begin 
  tarif := 5000;
end
else 
begin 
  tarif := 2000;
end;


CASE (kategori) OF 
'A' : harga := tarif + (total_jam - 1) * 1000;
'B' : harga := tarif + (total_jam - 1) * 2000;
'C' : harga := tarif + (total_jam - 1) * 3000;
end;

writeln('Yang harus anda bayar :',harga:2:0);

writeln('==========================');
write('Uang yang anda masukan : ');
readln(uang_masuk);

writeln('========loading===========');

kembalian := uang_masuk - harga ;

writeln('Kembalian anda : ',kembalian:2:0);

{ === End Main Program ====}
end.