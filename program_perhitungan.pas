(*
 * @Title       : Program Tabel Perkalian,Pembagian,Penjumlahan
 * @Matkul		: Pemrograman
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)

program perhitungan;
uses crt; // CRT.TPU 

{ ==== Start Deklarasi Variable ===== }
var 
 hasil_kali,hasil_jumlah,
 i : integer;
var 
 nilai1,nilai2 			   : integer;
var 
 hasil_bagi : real;
 
{ ==== End Variable ===== }

begin 
{ clrscr; }

i := 0;


repeat 
begin
clrscr;

writeln('             TABEL PERKALIAN, PEMBAGIAN , PENJUMLAHAN            ');
writeln('=================================================================');
writeln('| Nilai 1 | Nilai 2 | Hasil Perkalian | Pembagian | Penjumlahan |');
writeln('=================================================================');


for  nilai1 := 5 to 9  do 
begin
  for  nilai2 := 7 to 9  do 
    begin 
	  hasil_kali 	:= nilai1 * nilai2;
	  hasil_bagi 	:= nilai1 / nilai2;
	  hasil_jumlah	:= nilai1 + nilai2;
	  
	  writeln('     ',nilai1,'         ' , nilai2 , '           ', hasil_kali,'             ',hasil_bagi:0:2,'          ',hasil_jumlah);
	end;
end;

i := i + 1;
end;
until i < 5 ;
writeln('====================================================================');
write('Tekan Enter Untuk Keluar');
readln;
end.