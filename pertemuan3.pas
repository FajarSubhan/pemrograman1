(*
 * @Title		: Latihan soal pertemuan 3 
 * @Matkul		: Pemrograman
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)

program latihan_soal;
{ Import Library CRT.TPU }
uses crt;

{ Deklaration Variable }
var 
  bilangan , tahun , hari : integer;

  
{ Deklaration Procedures }
procedure check( type_value : string);
begin 
  {* === Start Soal Nomer 1 === *}
  if( type_value = 'ganjilgenap' ) then
  begin 
    { bilangan genap adalah bilangan yang akan habis jika dibagi 2 }
	if( bilangan mod 2 = 0 ) then 
	begin 
	  writeln(bilangan , ' adalah bilangan ', upcase('genap'));
	end
	{ bilangan ganjil adalah bilangan yang tidak akan habis jika dibagi 2 }
	else if( bilangan mod 2 <> 0 ) then
	begin
	  writeln(bilangan , ' adalah bilangan ' , upcase('ganjil'));
	end
	{ jika tidak ada diantara keduanya } 
	else
	begin
	  writeln('bilangan tidak diketahui');
	end;
	
  end
  {* === End Soal Nomer 1 === *}
  
  
  {* === Start Soal Nomer 2 === *}
  else if ( type_value = 'kabisat' ) then 
  begin 
  (*
   * a.Jika angka tahun itu habis dibagi 400, maka tahun itu sudah pasti tahun kabisat.
   * b.Jika angka tahun itu tidak habis dibagi 400 tetapi habis dibagi 100, maka tahun itu sudah pasti bukan merupakan tahun kabisat.
   * c.Jika angka tahun itu tidak habis dibagi 400, tidak habis dibagi 100 akan tetapi habis dibagi 4, maka tahun itu merupakan tahun kabisat.
   * d.Jika angka tahun tidak habis dibagi 400, tidak habis dibagi 100, dan tidak habis dibagi 4, maka tahun tersebut bukan merupakan tahun kabisat.
  *)
    if( tahun mod 400 = 0) then 
	begin 
      writeln(tahun ,' merupakan tahun ', upcase('kabisat'));
	end
    else if( 
	tahun mod 100 = 0 ) then 
	begin 
	  writeln(tahun , ' bukan tahun ', upcase('kabisat'));
	end
	else if( tahun mod 4 = 0 ) then 
	begin 
	  writeln(tahun , ' merupakan tahun ', upcase('kabisat'));
	end
	else 
	begin
	  writeln(tahun , ' bukan tahun ', upcase('kabisat'));
	end;
	
  end
  {* === End Soal Nomer 2 === *}
  
  {* === Start Soal Nomer 3 === *}
  else if( type_value = 'hari') then 
  begin 
    case ( hari ) of 
	0 : writeln('Hari Minggu');
	1 : writeln('Hari Senin');
	2 : writeln('Hari Selasa');
	3 : writeln('Hari Rabu');
	4 : writeln('Hari Kamis');
	5 : writeln('Hari Jumat');
	6 : writeln('Hari Sabtu');
  otherwise 
   writeln('Hari tidak valid');
   end;
  end;
  (* === End Soal Nomer 3 ===== *)
  
end;

{ ===== Main Program ======= }
begin 
{ clear screen }
clrscr;

(*
 * ================================== ### Start no 1 ### ============================================
 * 1.Buatlah program untuk menentukan suatu bilangan yang dimasukkan oleh user,merupakan bilangan genap atau ganjil
*)
write('1.Silahkan masukan nomer bilangan : ');
readln(bilangan);

check( 'ganjilgenap'); // call procedures

writeln('========================================');
(* ================================== ### End no 1 ### ============================================ *)
 
 
(* ================================== ### Start no 2 ### ============================================ 
 * 2. Buatlah sebuah program untuk menentukan bilangan tahun yang dimasukkan oleh user merupakan bikangan tahun kabisat atau bukan
*)
 write('2.Silahkan masukan tahun : ');
 readln(tahun);
 
 check( 'kabisat');
 
 writeln('========================================');
(* ================================== ### End no 2 ### ============================================ *)

(* 
 *
 * ================================== ### Start no 3 ### ============================================ 
 * Buatlah sebuah program yang akan meneriman masukan bilangan bulat dari
 * user. Jika bilangan yang dimasukkan 0, maka program akan menampilkan string
 * ‘Minggu’; jika user memasukkan bilangan 1, maka program akan menampilkan
 * string ‘Senin’; dan seterusnya sampai Sabtu; jika user memasukkan bilangan
 * lebih dari 6, maka akan keluar string ‘Hari tidak valid’.
 *
*)
write('3.Silahkan masukan angka hari ( 0 = minggu , 1 = senin ) : ');
readln(hari);

check('hari');


(* ================================== ### End no 3 ### ============================================ *)

 
 
end.

