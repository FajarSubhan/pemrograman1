(*
 * @Title       : Latihan bilangan deret dengan pascal
 * @Matkul		: Algoritma
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)

{* #FYI : 
 * [0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,...]
 * Penjelasan: barisan ini berawal dari 0 dan 1, 
 * kemudian angka berikutnya didapat dengan cara menambahkan kedua bilangan yang berurutan sebelumnya
 * Docs @https://id.wikipedia.org/wiki/Bilangan_Fibonacci
 *}


program fibonacci;
uses crt; // crt.tpu unit

{ #Declarasi Variable }

var 
  a , b , c : real;

var 
  x , t  : integer;
  
begin 
clrscr;

{ #Inisialisasi Variabel : Proses Isi nilai awal }
a := 0; // bilangan ke 1
b := 1;	// bilangan ke 2

c := 0;
t := 0;

writeln('==================================');
writeln('#         PROGRAM FIBONACCI      #');
writeln('==================================');

writeln;

write('Masukan angka deret sebanyak : '); // # Input nilai x
readln(x);

writeln('==================================');


write('Deretnya : '); // deretnya dihitung dari mulai angka 0

while t < x do // #kondisi apakah t kurang dari x dan terus di looping sampai kondisi false
  begin 
    c := a + c;
    a := b;
    b := c;
	
    t := t + 1;
    write(c:0:0,' ');
  end;
end.