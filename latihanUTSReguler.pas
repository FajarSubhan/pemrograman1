(*
 * @Title       : Soal-Soal UTS Kelas Reguler
 * @Matkul		: Pemrograman
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)


program uts_reguler;
uses crt;

{ ============#Start Deklrasi Variable NO 1 #============= }
var 
  nilai1,nilai2,nilai3 : integer;

var 
  check_kelipatan,check_perbandingan : string;
{ ============#End Variable NO 1 #================== }

{ ============#Start Variable NO 2 #================== }
var 
  kep_kel 			: string;

var  
  jum_anak,
  peng_perbulan,
  listrik_perbulan,	
  sub_pendidikan,
  bant_sosial 		: QWord;
 
  
{ ============#End Variable NO 2 #================== }

{ ============#Start Variable NO 3 #================== }
var 
  jen_kend,paket : string;

var 
  harga  : QWord;
{ ============#End Variable NO 3 #================== }
begin 
clrscr;

{ ========================# Start No 1 : Mencari kelipatan dan lebih besar #========================}
writeln('Soal No 1 : Mencari kelipatan dan lebih besar');
writeln('-------------------------------------------------');

write('Masukan Nilai 1 = ');
readln(nilai1);

write('Masukan Nilai 2 = ');
readln(nilai2);

write('Masukan Nilai 3 = ');
readln(nilai3);

(* Apakah nilai 1 kelipatan dari nilai 2 *)
 if(nilai1 > 0) and (nilai2 > 0) then 
 begin 
   if(nilai1 mod nilai2 = 0 ) then 
   begin 
	check_kelipatan := 'TRUE';
   end
   else 
   begin 
	check_kelipatan := 'FALSE';
   end;  
 end;
 
(* Apakah benar nilai 3 lebih besar dari nilai 2 *)
if(nilai3 > nilai2) then
begin
  check_perbandingan := 'TRUE';
end
else 
begin 
  check_perbandingan := 'FALSE';
end;


// Output
writeln;

writeln('|===================================================|');
writeln('Apakah Benar Nilai 1 kelipatan dari Nilai 2 ? = ',check_kelipatan);
writeln('|===================================================|');

writeln('|===================================================|');
writeln('Apakah Benar Nilai 3 lebih besar dari Nilai 2 = ',check_perbandingan);
writeln('|===================================================|');

{ ========================# End No 1 #======================== }

writeln;
writeln;

{ ==================# Start No 2 : IF Majemuk #===================}
writeln('Soal No 2 : IF Majemuk');
writeln('--------------------------');

write('Nama Kepala Keluarga              : ');
readln(kep_kel);

write('Jumlah Anak Usia ( 6 - 17 Tahun ) : ');
readln(jum_anak);

write('Penghasilan Perbulan              : ');
readln(peng_perbulan);

write('Konsumsi Listrik Tiap Bulan       : ');
readln(listrik_perbulan);

if(peng_perbulan >= 2000000) and (listrik_perbulan >= 100000) then 
begin 
  sub_pendidikan := 300000;
  bant_sosial    := 200000;
end
else if(peng_perbulan > 2000000) and (listrik_perbulan < 100000) then 
begin 
  sub_pendidikan := 100000;
  bant_sosial	 := 100000;
end
else if(peng_perbulan <= 2000000) and (listrik_perbulan >= 100000) then 
begin 
  sub_pendidikan := 100000;
  bant_sosial    := 100000;
end
else if(peng_perbulan < 2000000) and (listrik_perbulan < 100000) then 
begin 
  sub_pendidikan := jum_anak*700000;
  bant_sosial    := jum_anak*400000;
end
else 
begin
  sub_pendidikan := 0;
  bant_sosial    := 0;
end;

writeln;

// output
writeln('TOTAL DARI ',jum_anak,' ANAK');

// Subsidi pendidikan 
writeln('Besaran Subsidi Pendidikan      : ',sub_pendidikan);
writeln('Besaran Bantuan Sosial          : ',bant_sosial);

{ ========================# End No 2 #========================}

writeln;
writeln;

{ ========================# Start No 3 : IF Bersarang / Nested IF #===================== }
writeln('Soal No 3 : IF Bersarang / Nested IF');
writeln('-----------------------');
writeln('====== CUCI Cuci Steam ========');
writeln('Paket1 ==> Wax,Cuci,Poles');
writeln('Paket2 ==> Wax,Cuci');
writeln('Paket3 ==> Cuci Bersih');
writeln('================================');
write('Apa jenis kendaraan anda (mt(motor) atau mb(mobil)) : ');
readln(jen_kend);

write('Pilih Paket : ');
readln(paket);

if(jen_kend = 'mt') then 
begin 
  if(paket = 'Paket1') then
  begin   
   harga := 100000;
  end 
  else if(paket = 'Paket2') then 
  begin 
   harga := 80000;
  end 
  else if(paket = 'Paket3') then 
  begin
    harga := 50000;
  end;
end 
else if(jen_kend = 'mb') then 
begin 
  if(paket = 'Paket1') then 
  begin 
    harga := 450000;
  end
  else if(paket = 'Paket2') then 
  begin 
    harga := 350000;
  end
  else if(paket = 'Paket3') then 
  begin
    harga := 150000;
  end
end;


writeln('===============================');
writeln('Harga : ',harga);

{ ========================# End No 3 #===================== }
end.