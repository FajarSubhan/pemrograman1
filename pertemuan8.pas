(*
 * @Title		: Latihan soal pertemuan 8
 * @Matkul		: Pemrograman
 * @CreatedBy	: Fajar Subhan
 * @Nim			: 202043500578
 * @License 	: The GNU General Public License (GNU GPL or simply GPL) https://www.gnu.org/
 * @Docs		: https://www.freepascal.org/docs.html
*)

// Compiler Mode : Object FreePascal mode
{$mode objfpc}
program case_of;

{ Import Library CRT.TPU }
uses crt;

{ Deklaration Variable }
var 
  suhu,pilihan,a,t,r : integer;
var 
  confirm 		   : char;

{ Deklaration Function }

function convertSuhu( type_suhu : string; suhu_input : integer ) : real;
begin 
   case ( type_suhu ) of 
  'kelvin'  : { Celcius -> Kelvin }
    begin 
	  //  K = ° C + 273.15
	  convertSuhu := suhu_input + 273.15;
	end;
	
   'reamur' : {  Celcius -> Reamur }
     begin 
	   // R = (4/5) C
	   convertSuhu := (4/5)*suhu_input;
	 end;
   otherwise
     convertSuhu := 0;
   end;
end;


function luas( type_luas : string ; alas : integer  = 0 ; tinggi : integer = 0; jari_jari : integer = 0  ) : real;
const 
  PHI = 3.14;

begin 
  {*
   * LS = 'Luas Segitiga'
   * LL = 'Luas Lingkaran'
  *}
  case ( type_luas ) of 
  'LS'  : // L = ½ x alas x tinggi
    begin
      luas := (alas * tinggi) / 2; 	  
	end;
  'LL'  : // L = π x r2 
    begin 
	  luas := PHI * jari_jari * jari_jari; 
	end;
  otherwise 
    luas := 0;
  end;
	
end;
  
  
(* ==== Start Main Program ===== *)
begin 
clrscr;

(* ============= [Start] soal No 1. Konversi Suhu celsius ke kelvin dan reamur ============== *)
repeat 

writeln('==============================');
writeln('#    Program Konversi Suhu   #');
writeln('==============================');

writeln;

writeln('* Pilihan Konversi : ');
writeln('_______________________________');
writeln;
writeln('[1] Konversi Celcius -> Kelvin');
writeln('[2] Konversi Celcius -> Reamur');
writeln;
write('Masukan nomer pilihan anda : ');
readln(pilihan);

case ( pilihan ) of 
1 : // celcius to kelvin 
  begin 
    write('Silahkan masukan angka suhu : ');
	readln(suhu);
	
	
	writeln('Suhu dalam kelvin = ',convertSuhu('kelvin',suhu):0:2 , 'K');
  end;

2 : // celcius to reamur 
  begin 
    write('Silahkan masukan angka suhu : ');
	readln(suhu);
	
	writeln('Suhu dalam reamur = ',convertSuhu('reamur',suhu):0:0, 'Re');
  end;
otherwise
  writeln;
  writeln('Mohon maaf,konversi nomer ', pilihan,' untuk saat ini tidak tersedia');
end;

// check confirmation on looping repeat until 
writeln;
write('Apakah anda ingin melanjutkan konversi suhu ? (Y/N) ');
readln(confirm);
writeln;
until UPCASE(confirm) <> 'Y';
(* ============= [End] soal No 1. Konversi Suhu celsius ke kelvin dan reamur ============== *)


(* ================= [Start] soal No 2. Mencari Luas Segitiga ========================= *)
repeat 
  writeln('==============================');
  writeln('#    Program Hitung Luas     #');
  writeln('==============================');
  writeln;

writeln('* Pilihan Perhitungan Luas : ');
writeln('_______________________________');
writeln;
writeln('[1] Luas Segitiga');
writeln('[2] Luas Lingkaran');
writeln;
write('Masukan pilihan anda [ 1 | 2 ] : ');
readln(pilihan);

case ( pilihan ) of 
1 : // Luas Segitiga
  begin 
    write('Masukan nilai alas   : ');
	readln(a);
	write('Masukan nilai tinggi : ');
	readln(t);
	
	writeln('Luas segitiga = ',luas('LS',a,t):0:0 ,'Cm');
  end;
2 : // Luas Lingkaran
  begin 
	write('Masukan angka r / jari-jari : ');
	readln(r);
	
	writeln('Luas lingkaran = ',luas('LL',0,0,r):0:0,'Cm');
  end;

otherwise
  writeln;
  writeln('Mohon pilihan [', pilihan,'] untuk saat ini tidak tersedia');
end;

// check confirmation on looping repeat until 
writeln;
write('Apakah anda ingin melanjutkan hitung luas ? (Y/N) ');
readln(confirm);
writeln;
until UPCASE(confirm) <> 'Y'; 
(* ================= [End] soal No 2. Mencari Luas Segitiga ========================= *)
 
end.
(* ==== Start Main Program ===== *)